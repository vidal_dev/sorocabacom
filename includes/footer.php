
<footer id="rodape">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<a href="#top" class="glyphicon glyphicon-menu-up"></a>
			</div>
		</div>
	</div>
</footer>

<script src="js/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('a[href="#top"]').fadeIn();
			} else {
				$('a[href="#top"]').fadeOut();
			}
		});

		$('a[href="#top"]').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});
	});
</script>

<script>

	function parallax(){
			// Declarando as var.
			var layer_1 = document.getElementById('layer_1');
			var layer_2 = document.getElementById('layer_2');
			var layer_3 = document.getElementById('layer_3');
			var layer_4 = document.getElementById('layer_4');

			// Aplicando a posição
			layer_1.style.top = -(window.pageYOffset / 3) + 'px';
			layer_2.style.top = -(window.pageYOffset / 6) + 'px';
			layer_3.style.top = -(window.pageYOffset / 7) + 'px';
			layer_4.style.top = -(window.pageYOffset / 7) + 'px';
		}
		window.addEventListener("scroll", parallax, false);

	</script>
</body>
</html>

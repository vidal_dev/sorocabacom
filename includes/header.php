<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6 demo-2 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7 demo-2 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8 demo-2 no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="pt-br">
<!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Sorocaba Com - Teste</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Metas SEO -->
	<meta property="og:url"                content="" />
	<meta property="og:type"               content="article" />
	<meta property="og:title"              content="" />
	<meta property="og:description"        content="" />
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

	<script src="js/modernizr.custom.js"></script>
    
    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->

</head>
<body id="all">
    <!-- Google Tag Manager (noscript) -->

    <!-- End Google Tag Manager (noscript) -->

	<header id="header" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="logo">
                        <span><img src="images/Imagem3.png"></span>
                        <h1>SuperGiantGames</h1>
                    </div>
                </div>
            </div>
        </div>
	</header>

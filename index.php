<?php include 'includes/header.php'?>
<section id="banner">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="item">
          <img src="images/Imagemcard.png">

          <h2>
            "Olha, o que quer que você esteja pensando, me faça um favor, não solte."
          </h2>

          <section>
            <div id="layer_3" class="img-parallax"></div>
            <div id="layer_4" class="img-parallax"></div>
            <div id="layer_1" class="img-parallax"></div>
            <div id="layer_2" class="img-parallax"></div>
          </section>
        </div>
        <div class="scroll-downs">
          <div class="mouse">
            <div class="scroller"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="cards">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="item">
                <div class="image">
                  <img src="images/Grant.png">
                </div>
                <div class="content">
                  <h3>A Camerata foi apenas os dois no início, e suas fileiras nunca foram destinadas a exceder um número a ser contado em uma mão.</h3>
                </div>
              </div>

              <div class="item">
                <div class="image">
                  <img src="images/Red.png">
                </div>
                <div class="content">
                  <h3>Red, uma jovem cantora, entrou em posse do Transistor. Sendo a poderosa espada falante. O grupo Possessores quer tanto ela quanto o Transistor e está perseguindo implacavelmente a sua procura.</h3>
                </div>
              </div>

              <div class="item">
                <div class="image">
                  <img src="images/Sybil_2.png">
                </div>
                <div class="content">
                  <h3>Sybil é descrita pelo Transistor como sendo os "olhos e ouvidos" da Camerata.</h3>
                </div>
              </div>
            </div>

          </div>
          <a class="carousel-control-prev glyphicon glyphicon-menu-left" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next glyphicon glyphicon-menu-right" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="contato">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">

        <div class="box_form clearfix">
          <h2>FORMULÁRIO</h2>

          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

          <form>
            <input type="text" name="nome" placeholder="Nome" required>
            <input style="margin-right: 0;" type="email" name="email" placeholder="Email" required>

            <textarea placeholder="Mensagem" required></textarea>

            <button>Enviar</button>

          </form>
        </div>

      </div>
    </div>
  </div>
</section>
<?php include 'includes/footer.php'?>
